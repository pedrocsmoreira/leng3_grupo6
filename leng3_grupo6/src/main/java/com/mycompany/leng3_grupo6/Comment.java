/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leng3_grupo6;

import com.mycompany.utilitarios.Data;
import java.util.Scanner;

/**
 *
 * @author grupo6
 */
public class Comment {
    private int commentID;
    private int parentComment;
    private int postID;
    private int userID;
    private String commentText;
    private Data commentData;
    
    private static final int INT = 0;
    private static final String STRING = "";
    private static final Data DATA = new Data();

    public Comment() {
       this.commentID = INT;
        this.parentComment = INT;
        this.postID = INT;
        this.userID = INT;
        this.commentText = STRING;
        this.commentData = DATA; 
    }
    
    public Comment(int commentID, int parentComment, int postID, int userID, String commentText, Data commentData) {
        this.commentID = commentID;
        this.parentComment = parentComment;
        this.postID = postID;
        this.userID = userID;
        this.commentText = commentText;
        this.commentData = commentData;
    }

    
    public int getCommentID() {
        return commentID;
    }

    public int getParentComment() {
        return parentComment;
    }

    public int getPostID() {
        return postID;
    }

    public int getUserID() {
        return userID;
    }

    public String getCommentText() {
        return commentText;
    }

    public Data getCommentData() {
        return commentData;
    }

    
    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public void setParentComment(int parentComment) {
        this.parentComment = parentComment;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public void setCommentData(Data commentData) {
        this.commentData = commentData;
    }

    @Override
    public String toString() {
        return "Comment{" + "commentID=" + commentID + ", parentComment=" + parentComment + ", postID=" + postID + ", userID=" + userID + ", commentText=" + commentText + ", commentData=" + commentData + '}';
    }
    
}
