package com.mycompany.leng3_grupo6;

import com.mycompany.utilitarios.Data;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author grupo6
 */
public class Event {
    private int eventID;
    private int userID;
    private String eventName;
    private String eventDescription;
    private Date eventData;
    private ArrayList<User> userGuests;
    
    private static final int INT_DEFAULT = 0;
    private static final String STRING_DEFAULT = "";

    public Event() {
        this.eventID = INT_DEFAULT;
        this.userID = INT_DEFAULT;
        this.eventName = STRING_DEFAULT;
        this.eventDescription = STRING_DEFAULT;
        this.eventData = new Date();
        this.userGuests = new ArrayList<User>();
    }
    
    public Event(int eventID, int userID, String eventName, String eventDescription, Data eventData, ArrayList<User> userGuests) {
        this.eventID = eventID;
        this.userID = userID;
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.eventData = new Date();
        this.userGuests = userGuests;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Date getEventData() {
        return eventData;
    }

    public void setEventData(Date eventData) {
        this.eventData = eventData;
    }

    public ArrayList<User> getUserGuests() {
        return userGuests;
    }

    public void setUserGuests(ArrayList<User> userGuests) {
        this.userGuests = userGuests;
    }

    @Override
    public String toString() {
        return "Event{" + "eventID=" + eventID + ", userID=" + userID + ", eventName=" + eventName + ", eventDescription=" + eventDescription + ", eventData=" + eventData + ", userGuests=" + userGuests + '}';
    }
    
    
    
}
