package com.mycompany.leng3_grupo6;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author grupo6
 */

public class ManipulacaoFicheiro {
    private static  ArrayList<Object> arr;
    
    private static boolean gravarFicheiro(String nomeFicheiro, ArrayList<Object> arr) {
        try {
            FileOutputStream fout = new FileOutputStream(nomeFicheiro);
            ObjectOutputStream out = new ObjectOutputStream(fout);
            try {
                out.writeObject(arr);
                return true;
            }finally {
                out.close();
            }
        }catch (IOException ex) {
            return false;
        }
    }
    
    private static boolean lerFicheiro(String nomeFicheiro) {
        try {
            FileInputStream fin = new FileInputStream(nomeFicheiro);
            ObjectInputStream in = new ObjectInputStream(fin);
            try {
                arr =(ArrayList<Object>) in.readObject();
                return true;
            } finally {
                in.close();
            }
        }catch (FileNotFoundException ex) {
            System.out.println("Não conseguiu encontrar ficheiro");
            return false;
        }catch (IOException ex) {
            System.out.println("Erro na leitura do ficheiro");
            return false;
        }catch (ClassNotFoundException ex) {
            System.out.println("Erro no carregamento da classe!!");
            return false;
        }
    }

    public static boolean gravarInformacaoPessoas(String nomeFicheiro,ArrayList<Object> arr){
        return gravarFicheiro(nomeFicheiro, arr);
    }
    
    public static ArrayList<Object> lerInformacaoPessoas(String nomeFicheiro){
        if (lerFicheiro(nomeFicheiro))
            return arr;
        else
            return null;
    }
}