package com.mycompany.leng3_grupo6;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author grupo6
 */

public class Network {
    private static ArrayList<User> userList = new ArrayList<User>();
    private static ArrayList<Post> postList = new ArrayList<Post>();
    private static ArrayList<Event> eventList = new ArrayList<Event>();
    
    private static User logedUser = new User();
    
    private static ArrayList<User> myFriends = new ArrayList<User>();
    private static ArrayList<Post> myPosts = new ArrayList<Post>();
    private static ArrayList<User> birthDays = new ArrayList<User>();
    private static ArrayList<Event> eventInvite = new ArrayList<Event>();
    
    private static String networkName = "";
    
    public Network(){
        this.networkName = "";
    }
    
    public Network(String name){
        
    }
    
    public static User getLogedUser(){
        return logedUser;
    }
    
    public static ArrayList<User> getUserList(){
        return userList;
    }
    
    public static ArrayList<Post> getPostList(){
        return postList;
    }
    
    public static ArrayList<Event> getEventList(){
        return eventList;
    }
    
    public static ArrayList<User> getFriends(){
        return myFriends;
    }
    
    public static ArrayList<Post> getMyPosts(){
        return myPosts;
    }
    
    public static ArrayList<User> getBirthDay(){
        return birthDays;
    }
    
    public static ArrayList<Event> getEventInvite(){
        return eventInvite;
    }
    
    public static boolean processLogin(String username, String password){
        for(User u : userList){
            if(username.equals(u.getUsername()) && password.equals(u.getPassword())){
                logedUser = u;
                for(User f : userList){
                    if(true){
                       myFriends.add(f);
                       if(f.getBirthDate().compareTo(systemDate()) == 0){
                           birthDays.add(f);
                       }
                    }
                }
                for(Post p : postList){
                    if(p.getUserID() == logedUser.getUserID()){
                        myPosts.add(p);
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    private static void processSignup(String signUpData, String pageData){
        String[] signUpSplit = signUpData.split("*");
        String[] pageDataSplit = pageData.split("*");
        userList.add(new User());
    }
    
    public static ArrayList<Post> publicationFeed(){
        ArrayList<Post> posts = new ArrayList<Post>();
        for(User u : userList){
            
        }
        return posts;
    }
    
    public static ArrayList<User> searchProfile(String user){
        ArrayList<User> searched = new ArrayList<User>();
        for(User u : userList){
            if(u.getName().contains(user) || u.getUsername().contains(user)){
                searched.add(u);
            }
        }            
        return searched;
    }
    
    public static void post(String text){
        postList.add(new Post(logedUser.getUserID(), text, systemDate()));
    }
    
    public static void logout(){
        logedUser = new User();
        myFriends = new ArrayList<User>();
        myPosts = new ArrayList<Post>();
        birthDays = new ArrayList<User>();
        eventInvite = new ArrayList<Event>();
    }
    
    
    
    
    private static Date systemDate(){
        Date date = new Date();
        return date;
    }
    
}

