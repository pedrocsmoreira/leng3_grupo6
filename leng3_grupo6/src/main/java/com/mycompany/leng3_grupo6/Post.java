package com.mycompany.leng3_grupo6;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author grupo6
 */

public class Post {
    private int postID;
    private int userID;
    private String postText;
    private Date postData = new Date();
    private static ArrayList<Comment> comments = new ArrayList<>();
    
    private static int counter = 0;
    private static final int USERID_POR_OMISSAO = 0;
    private static final String POSTTEXT_POR_OMISSAO = "";

    public Post () {
        this.postID = ++counter;
        this.userID = USERID_POR_OMISSAO;
        this.postText = POSTTEXT_POR_OMISSAO;
        this.postData= new Date();
        this.comments = new ArrayList<>();
    }

    public Post(int userID, String postText,Date postData) {
        this.postID = ++counter;
        this.userID = userID;
        this.postText = postText;
        this.postData = postData;
    }
    
    public Post(int userID, String postText,Date postData, ArrayList<Comment> comments) {
        this.postID = ++counter;
        this.userID = userID;
        this.postText = postText;
        this.postData = postData;
        this.comments = comments;
    }

    public int getUserID() {
        return userID;
    }

    public String getPostText(){
        return postText;
    }
    
    public Date getPostDate (){
        return postData;
    }
    
    public ArrayList<Comment> getComment () {  
        return comments;
    }
    
    public void setUserID (int userID) {
        this.userID = userID;
    }
    
    public void setPostText(String postText) {
        this.postText = postText;
    }
    
    public void setPostDate(Date postData) {
        this.postData = postData;
    }
    
    public void setComment(ArrayList<Comment> comment) {
        this.comments = comment;
    }

    @Override
    public String toString() {
        return "Post{" + "userID=" + userID + ", postText=" + postText + ", postData=" + postData + ", comments=" + comments + '}';
    }
}