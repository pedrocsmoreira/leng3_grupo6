/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leng3_grupo6;
import com.mycompany.utilitarios.Data;

/**
 *
 * @author 35191
 */
public class React {
    private int reactionID;
    private int postID;
    private int userID;
    private String reaction;
    private Data reactionData;
    
    private static final int INT = 0;
    private static final String STRING = "";
    private static final Data DATA = new Data();
    
    public React() {
        this.reactionID = INT;
        this.postID = INT;
        this.userID = INT;
        this.reaction = STRING;
        this.reactionData = DATA;
    }

    public React(int reactionID, int postID, int userID, String reaction, Data reactionData) {
        this.reactionID = reactionID;
        this.postID = postID;
        this.userID = userID;
        this.reaction = reaction;
        this.reactionData = reactionData;
    }

    public int getReactionID() {
        return reactionID;
    }

    public int getPostID() {
        return postID;
    }

    public int getUserID() {
        return userID;
    }

    public String getReaction() {
        return reaction;
    }

    public Data getReactionData() {
        return reactionData;
    }

    public void setReactionID(int reactionID) {
        this.reactionID = reactionID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public void setReactionData(Data reactionData) {
        this.reactionData = reactionData;
    }

    @Override
    public String toString() {
        return "React{" + "reactionID=" + reactionID + ", postID=" + postID + ", userID=" + userID + ", reaction=" + reaction + ", reactionData=" + reactionData + '}';
    }
    
    
    
}
