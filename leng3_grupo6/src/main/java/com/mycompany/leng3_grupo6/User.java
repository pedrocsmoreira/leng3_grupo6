package com.mycompany.leng3_grupo6;

import java.util.Date;

/**
 *
 * @author grupo6
 */
public class User {
    private static int userID;
    private static String username;
    private static String name;
    private static String email;
    private static String password;
    private static String nacionality;
    private static String gender;
    private static Date birthDate;
    private static String biography;
    private static boolean visibility;
    private static Date signupDate;
    
    private static int userNumber = 0;
    private static final String STRING_DEFAULT = "";
    private static final int INT_DEFAULT = 0;
    private static final boolean BOOL_DEFAULT = true; 
    
    public User(){
        this.userID = ++userNumber;
        this.username = STRING_DEFAULT;
        this.name = STRING_DEFAULT;
        this.email = STRING_DEFAULT;
        this.password = STRING_DEFAULT;
        this.nacionality = STRING_DEFAULT;
        this.gender = STRING_DEFAULT;
        this.birthDate = new Date();
        this.biography = STRING_DEFAULT;
        this.visibility = BOOL_DEFAULT;
        this.signupDate = new Date();
    }
    
    public User(String username, String name, String email, String password, String nacionality, String gender, Date birthDate, String biography, Boolean visibility, Date signupDate){
        this.userID = ++userNumber;
        this.username = username;
        this.name = name;
        this.email = email;
        this.password = password;
        this.nacionality = nacionality;
        this.gender = gender;
        this.birthDate = birthDate;
        this.biography = biography;
        this.visibility = visibility;
        this.signupDate = signupDate;
    }
    
    public int getUserID() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNacionality() {
        return nacionality;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setNacionality(String nacionality) {
        this.nacionality = nacionality;
    }

    public static String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public static Date getBirthDate() {
        return birthDate;
    }

    public static void setBirthDate(Date birthDate) {
        User.birthDate = birthDate;
    }
    
    public static String getBiography() {
        return biography;
    }

    public static void setBiography(String biography) {
        User.biography = biography;
    }

    public static boolean isVisibility() {
        return visibility;
    }

    public static void setVisibility(boolean visibility) {
        User.visibility = visibility;
    }
    
    public static Date getSignupDate() {
        return signupDate;
    }

    public static void setSignupDate(Date signupDate) {
        User.signupDate = signupDate;
    }

}