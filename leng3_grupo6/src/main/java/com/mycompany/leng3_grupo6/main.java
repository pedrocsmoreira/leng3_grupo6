package com.mycompany.leng3_grupo6;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.util.Date;

/**
 *
 * @author grupo6
 */

public class main {

    private static Scanner scan = new Scanner(System.in);
    private static Formatter format = new Formatter(System.out);
    
    private static Network network = new Network();
    
    public static void main(String[] args) {
        System.out.println("***** ------------------ *****");
        System.out.println("***** Welcome to Circles *****");
        System.out.println("***** ------------------ *****");
        System.out.println("");
        processingMainMenu();
    }
    
    private static void processingMainMenu(){
        int op;
        mainMenu();
        do{
            op = scan.nextInt();
            switch(op){
                case 1: if(login()){ processingOnlineMenu(); }; break;
                case 2: signup(); break;
                case 3: exit(); break;
                default: System.out.println("Invalid Option");
            }
        }while(op > 3);
    }
    
    private static void mainMenu(){
        System.out.println("1 - Login");
        System.out.println("2 - Sign Up");
        System.out.println("3 - Exit");
    }
    
    private static void processingOnlineMenu(){
        int op;
        mainMenu();
        do {
            op = scan.nextInt();
            switch(op){
                case 1: publicationFeed(); break;
                case 2: searchProfile(); break; 
                case 3: post(); break;
                case 4: notificationFeed(); break;
                case 5: myProfile(); break;
                case 6: logout();  break;
                default: System.out.println("Invalid Option");
            }
        }while(op > 6);
    }
    
    private static void onlineMenu(){
        System.out.println("*** Welcome to Circles, " + network.getLogedUser().getName() + " ***");
        System.out.println("1 - Publication Feed");
        System.out.println("2 - Search profile");
        System.out.println("3 - Post");
        System.out.println("4 - Notification Feed");
        System.out.println("5 - My profile");
        System.out.println("6 - Logout");
    }

    
    private static boolean login(){
        System.out.println("***** Login to Circles *****");
        System.out.println("Username: ");
        String username = scan.nextLine();
        System.out.println("Password: ");
        String password = scan.nextLine();
        password = passwordMD5(password);
        return network.processLogin(username, password);
    }
    
    private static String signup(){
        String signUp = "";
        signUp += signUpData();
        signUp += "*";
        signUp += pageData();
        return signUp;
    }
    
    private static String signUpData(){
        String signUpData = null;
        System.out.println("***** SignUp to Circles *****");
        System.out.println("Username: ");
        signUpData += scan.nextLine();
        signUpData += "*";
        System.out.println("Name: ");
        signUpData += scan.nextLine();
        signUpData += "*";
        System.out.println("Email: ");
        signUpData += scan.nextLine();
        signUpData += "*";
        System.out.println("Password: ");
        String password = scan.nextLine();
        password = passwordMD5(password);
        signUpData += password;
        signUpData += "*";
        System.out.println("Nacionality: ");
        signUpData += scan.nextLine();
        signUpData += "*";
        System.out.println("Gender: ");
        signUpData += scan.nextLine();
        return signUpData;
    }
    
    private static String pageData(){
        String pageData = null;
        System.out.println("***** Create your page *****");
        System.out.println("Biography: ");
        pageData += scan.nextLine();
        String visibility = "";
        do{
            System.out.println("Public page? (yes/no)");
            visibility += scan.nextLine();
        }while(visibility != "yes" || visibility != "no");
        pageData += "*";
        pageData += visibility;
        return pageData;
    }
    
    private static void publicationFeed(){
        ArrayList<Post> posts = network.publicationFeed();
        System.out.println("*** Post Feed ***");
        for(Post p : posts){
            System.out.println(p.toString());
        }
    }
    
    private static void searchProfile(){
        ArrayList<User> users = new ArrayList<User>();
        System.out.println("Search profile.... ");
        String user = scan.nextLine();
        users = network.searchProfile(user);
        for(User u : users){
            System.out.println(u.toString());
        }
    }
    
    private static void post(){
        System.out.println("Create a new post....");
        String text = scan.nextLine();
        network.post(text);
    }

    private static void notificationFeed(){
        
    }
    
    private static void myProfile(){
        System.out.println(network.getLogedUser().getName() + " - " +  network.getLogedUser().getUsername());
        System.out.println(network.getLogedUser().getNacionality());
        System.out.println("---------------------------------");
        System.out.println("Posts");
        for(Post p : network.getMyPosts()){
            System.out.println();
            System.out.println(network.getLogedUser().getName() + " - " + network.getLogedUser().getUsername());
            System.out.println(p.getPostText());
            System.out.println(p.getPostDate());
        }
    }
    
    private static void logout(){
        network.logout();
    }
    
    private static void exit(){
        
        System.exit(0);
    }
    
    private static String passwordMD5(String password){
        String encryptedPassword = null;  
        try{
            MessageDigest m = MessageDigest.getInstance("MD5");  
            m.update(password.getBytes());
            byte[] bytes = m.digest();
            StringBuilder s = new StringBuilder();  
            for(int i=0; i< bytes.length ;i++) {
                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));  
            }
            encryptedPassword = s.toString();
        }catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encryptedPassword;
    }
    private static Date systemDate(){
        Date date = new Date();
        return date;
    }
}
